---
layout: single
title: UPFG/PEGAS Thrust Integrals and Predictor
---

### Thrust Integrals

{%raw%}
$$
 \begin{align}
 L & = \int_0^{t_{go}} \frac fm \, dt \\
 J & = \int_0^{t_{go}} \frac fm t \, dt \\
 H & = \int_0^{t_{go}} \frac fm t^2 \, dt \\
 S & = \int_0^{t_{go}} \int_0^t \frac fm \, ds \, dt \\
 Q & = \int_0^{t_{go}} \int_0^t \frac fm s \, ds \, dt \\
 P & = \int_0^{t_{go}} \int_0^t \frac fm s^2 \, ds \, dt \\
 \end{align}
$$
{%endraw%}

### Phi

{%raw%}
$$
 \begin{align}
  \lambda & = \mathrm{unit} [ v_{go} ] \\
  \hat i_f  & = \mathrm{unit} [ \lambda - t_{\lambda} \dot \lambda ] \\
  \phi & = \cos^{-1} \hat i_f \cdot \lambda
 \end{align}
$$
{%endraw%}

### Predictor

This expression is not directly integrable:

{%raw%}
$$
 \begin{align}
   v_{thrust} & = \int_0^{t_{go}} \frac fm [ \lambda \cos(\phi + \dot \phi t ) + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} \sin(\phi + \dot \phi t) ] \, dt \\
   r_{thrust} & = \int_0^{t_{go}} \int_0^t \frac fm [ \lambda \cos(\phi + \dot \phi t ) + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} \sin(\phi + \dot \phi t) ] \, ds \, dt
 \end{align}
$$
{%endraw%}

Using the Maclaurin/Taylor expansion:


{%raw%}
$$
 \begin{align}
   \sin ( \phi + \dot \phi t ) & = \phi + \dot \phi t \\
   \cos ( \phi + \dot \phi t ) & = 1 - ( \phi + \dot \phi t ) ^ 2 / 2 = 1 - \phi^2/2 - \phi \dot \phi t - \dot\phi^2 t^2 / 2
 \end{align}
$$
{%endraw%}

The predictor becomes:

{%raw%}
$$
 \begin{align}
   v_{thrust} & = \int_0^{t_{go}} \frac fm [ \lambda ( 1 - \phi^2/2 - \phi \dot \phi t - \dot\phi^2 t^2 / 2 ) + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} (\phi + \dot \phi t) ] \, dt \\
   r_{thrust} & = \int_0^{t_{go}} \int_0^t \frac fm [ \lambda (1 - \phi^2/2 - \phi \dot \phi t - \dot\phi^2 t^2 / 2 ) + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} (\phi + \dot \phi t) ] \, ds \, dt
 \end{align}
$$
{%endraw%}

Substituting thrust integrals:

{%raw%}
$$
 \begin{align}
   v_{thrust} & = [ L ( 1 - \phi^2/2 ) - J \phi \dot \phi - H \dot\phi^2 / 2 ) ] \lambda +  [ L \phi + J \dot \phi ] \mathrm{unit} ( \dot \lambda ) \\
   r_{thrust} & = [ S ( 1 - \phi^2/2 ) - Q \phi \dot \phi - P \dot\phi^2 / 2 ) ] \lambda +  [ S \phi + Q \dot \phi ] \mathrm{unit} ( \dot \lambda ) \\
 \end{align}
$$
{%endraw%}

Since $$\lambda$$ is chosen in the $$v_{thrust}$$ direction $$L \phi + J \dot \phi$$ can be set to zero:

{%raw%}
$$
 \begin{align}
   \dot \phi & = - \frac LJ \phi \\
   v_{thrust} & = [ L ( 1 - \phi^2/2 ) - J \phi \dot \phi - H \dot\phi^2 / 2 ) ] \lambda \\
   r_{thrust} & = [ S ( 1 - \phi^2/2 ) - Q \phi \dot \phi - P \dot\phi^2 / 2 ) ] \lambda +  [ S \phi + Q \dot \phi ] \mathrm{unit} ( \dot \lambda ) \\
 \end{align}
$$
{%endraw%}

Note that the UPFG no 24 paper includes the term for $$v_{thrust}$$ which must be zero, and appears to have a sign error in
the $$\mathrm{unit}(\dot \lambda)$$ expression for $$r_{thrust}$$.




