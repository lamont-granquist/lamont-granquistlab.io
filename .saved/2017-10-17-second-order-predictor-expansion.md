---
layout: single
title: Second Order Predictor Expansion
---

Notes for me, to get scribbles off my whiteboard

### Thrust Integrals

{%raw%}
$$
 \begin{align}
 L & = \int_0^{t_{go}} \frac fm \, dt \\
 J & = \int_0^{t_{go}} \frac fm t \, dt \\
 H & = \int_0^{t_{go}} \frac fm t^2 \, dt \\
 A & = \int_0^{t_{go}} \frac fm t^3 \, dt \\
 B & = \int_0^{t_{go}} \frac fm t^4 \, dt \\
 S & = \int_0^{t_{go}} \int_0^t \frac fm \, ds \, dt \\
 Q & = \int_0^{t_{go}} \int_0^t \frac fm s \, ds \, dt \\
 P & = \int_0^{t_{go}} \int_0^t \frac fm s^2 \, ds \, dt \\
 Y & = \int_0^{t_{go}} \int_0^t \frac fm s^3 \, ds \, dt \\
 Z & = \int_0^{t_{go}} \int_0^t \frac fm s^4 \, ds \, dt \\
 \end{align}
$$
{%endraw%}

### Predictor

Expanding:

{%raw%}
$$
 \begin{align}
   v_{thrust} & = \int_0^{t_{go}} \frac fm [ \lambda \cos(\phi + \dot \phi t ) + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} \sin(\phi + \dot \phi t) ] \, dt \\
   r_{thrust} & = \int_0^{t_{go}} \int_0^t \frac fm [ \lambda \cos(\phi + \dot \phi t ) + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} \sin(\phi + \dot \phi t) ] \, ds \, dt
 \end{align}
$$
{%endraw%}

Now to second order Taylor expansion of sin/cos:

{%raw%}
$$
 \begin{align}
   \sin ( \phi + \dot \phi t ) & = \phi + \dot \phi t - \frac{1}{6} ( \phi + \dot \phi t )^3 \\
   \cos ( \phi + \dot \phi t ) & = 1 - \frac{1}{2}( \phi + \dot \phi t ) ^ 2 + \frac{1}{24} ( \phi + \dot \phi t )^4
 \end{align}
$$
{%endraw%}

The predictors become:

{%raw%}
$$
 \begin{align}
   v_{thrust} & = \int_0^{t_{go}} \frac fm \{ \lambda [ 1 - \frac{\phi^2}{2} + \frac{\phi^4}{24} + ( \frac{\dot \phi \phi^3}{6} - \dot\phi \phi) t + ( \frac{\dot \phi^2 \phi^2}{4} - \frac{\dot \phi^2}{2})t^2 + \frac{\phi \dot \phi^3}{6}t^3 + \frac{\dot \phi^4}{24}t^4 ] \\
   & + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} [ ( \phi - \frac{\phi^3}{6} + ( \phi - \frac{\phi^2 \dot \phi}{2} ) t  - \frac{\phi \dot \phi^2}{2} t^2 - \frac{\dot\phi^3}{6}t^3 ] \} \, dt \\
   r_{thrust} & = \int_0^{t_{go}} \int_0^t  \frac fm \{ \lambda [ 1 - \frac{\phi^2}{2} + \frac{\phi^4}{24} + ( \frac{\dot \phi \phi^3}{6} - \dot\phi \phi) t + ( \frac{\dot \phi^2 \phi^2}{4} - \frac{\dot \phi^2}{2})t^2 + \frac{\phi \dot \phi^3}{6}t^3 + \frac{\dot \phi^4}{24}t^4 ] \\
   & + \frac{\dot \lambda}{\lvert \dot \lambda \rvert} [ ( \phi - \frac{\phi^3}{6} + ( \phi - \frac{\phi^2 \dot \phi}{2} ) t  - \frac{\phi \dot \phi^2}{2} t^2 - \frac{\dot\phi^3}{6}t^3 ] \} \, ds \, dt \\
 \end{align}
$$
{%endraw%}

Substituting thrust integrals:

{%raw%}
$$
 \begin{align}
   v_{thrust} & = [ L ( 1 - \frac{\phi^2}{2} + \frac{\phi^4}{24}) + J (\frac{\dot \phi \phi^3}{6} - \dot\phi \phi ) + H (\frac{\dot \phi^2 \phi^2}{4} - \frac{\dot \phi^2}{2} ) + A (\frac{\phi \dot \phi^3}{6} ) + B (\frac{\dot \phi^4}{24} )   ] \lambda \\
   & +  [ L (\phi - \frac{\phi^3}{6}) + J ( \phi - \frac{\phi^2 \dot \phi}{2}) - H (\frac{\phi \dot \phi^2}{2}) - A ( \frac{\dot\phi^3}{6}) ] \mathrm{unit} ( \dot \lambda ) \\
   v_{thrust} & = [ S ( 1 - \frac{\phi^2}{2} + \frac{\phi^4}{24}) + Q (\frac{\dot \phi \phi^3}{6} - \dot\phi \phi ) + P (\frac{\dot \phi^2 \phi^2}{4} - \frac{\dot \phi^2}{2} ) + Y (\frac{\phi \dot \phi^3}{6} ) + Z (\frac{\dot \phi^4}{24} )   ] \lambda \\
   & +  [ S (\phi - \frac{\phi^3}{6}) + Q ( \phi - \frac{\phi^2 \dot \phi}{2}) - P (\frac{\phi \dot \phi^2}{2}) - Y ( \frac{\dot\phi^3}{6}) ] \mathrm{unit} ( \dot \lambda ) \\
 \end{align}
$$
{%endraw%}

The $$\dot \lambda$$ side of the $$v_{thrust}$$ expression should be set to zero, and this equation will need to be solved for $$\dot\phi$$ given the thrust integrals and $$\phi$$:

{%raw%}
$$
 \begin{align}
 L (\phi - \frac{\phi^3}{6}) + J ( \phi - \frac{\phi^2 \dot \phi}{2}) - H (\frac{\phi \dot \phi^2}{2}) - A ( \frac{\dot\phi^3}{6}) = 0
 \end{align}
$$
{%endraw%}
