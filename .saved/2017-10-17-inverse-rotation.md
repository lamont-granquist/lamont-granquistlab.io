---
layout: single
title: Kerbal Space Program Inverse Rotation
---

## Unaffected APIs in MJ

- vesselState.CoM
- mainBody.position
- vesselState.orbitalVelocity
- vesselState.east / vesselState.north / vesselState.up

## Unaffected APIs in KSP:

- vessel.CoMD
- vessel.obt_velocity
- (probably anything not in the Orbit class?)

## To fix inverse rotation:

- use orbit.GetRotFrameVelAtPos to get the rotation
- orbit.GetRotFrameVelAtPos does not need the caller to do an altitude check and will return Vector3d.zero above the inverse rotation threshold
- it appears that getRotFrameVelAtPos checks the altitude of the ship to see if its above or below the threshold and that is what causes
  the universe to flip between inverse rot space and normal space.  as long as your ship is below the inv rot threshold i think you need
  to be using GetRotFrameVelAtPos for all vectors above and below the threshold.
- orbit.GetRotFrameVelAtPos uses vectors in swizzle-space as the rest of the orbit class does
- only the velocity vector needs to be updated, not the position (which still hurts my brain)

## Code to update an orbit and then extrapolate

Tested on 1.2.2:

```
Vector3d rot = orbit.GetRotFrameVelAtPos(mainBody, r0.xzy);
orbit.UpdateFromStateVectors(r0.xzy, v0.xzy + rot, mainBody, vesselState.time);
orbit.GetOrbitalStateVectorsAtUT(vesselState.time + t, out rf, out vf);

rot = orbit.GetRotFrameVelAtPos(mainBody, rf);

rf = rf.xzy;
vf = vf.xzy;
vf = (vf - rot).xzy;
``
