---
layout: single
title: Impulsive Single Burn Ejection via the Method of Lagrange Multipliers
---

Given a spacecraft at $$(\vec{r_0}, \vec{v_0})$$ find the minimum impulsive burn at
a later time $$t_1$$ with impulse $$\Delta\vec{v_1}$$ to eject onto a given
$$\vec{v}_{\infty}^*$$ hyperbolic velocity vector.

There are 4 optimization variables:

{%raw%}
$$
\vec{x_p} = ( t_1 \Delta\vec{v_1} )
$$
{%endraw%}

The dynamics of the coast are given by Goodyear's or Shepperd's method of Keplerian orbit propagation:

{%raw%}
$$
\vec{x_1^-} = \Psi(\vec{x_0}, t_1)
$$
{%endraw%}

The derivative of the state at $$t_1^-$$ is given by:

{%raw%}
$$
\dot{\vec{x_1^-}} = \begin{pmatrix} \vec{v_1^-} \\\ - \frac{\vec{r_1}}{\left\|r_1\right\|^3} \end{pmatrix}
$$
{%endraw%}

The sensitivities are given by the 6x6 State Transition Matrix, again given by Goodyear's or Shepperd's method:

FIXME: don't think we actually need this?

{%raw%}
$$
\begin{align}
\frac{\partial \vec{x_1^-}}{\partial \vec{x_0}} & = \Phi(\vec{x_0}, t_1) \\
\frac{\partial \vec{x_1^+}}{\partial t_1} & = \dot{\vec{x_1^-}}
\end{align}
$$
{%endraw%}

The partials of the final conditions with respect to the magnitude of the impulse are simply the identity matrix in the general case.  Since impulses have no $$\Delta r$$ that
partial is zero:

{%raw%}
$$
\frac{\partial \vec{x_1^+}}{\partial \Delta\vec{v_1}} = I_{6x6} \\
\frac{\partial \vec{r_1}}{\partial \Delta\vec{v_1}} = 0_{3x3} \\
\frac{\partial \vec{v_1^+}}{\partial \Delta\vec{v_1}} = I_{3x3}
$$
{%endraw%}

The cost metric is the magnitude of the impulse:

{%raw%}
$$
J = \left\| \Delta\vec{v_1} \right\|
$$
{%endraw%}

The Lagrangian of the problem is:

{%raw%}
$$
\mathscr{L} = \left\| \Delta\vec{v_1} \right\| - \vec{\lambda_{v_\infty}} \cdot ( \vec{v_\infty^*} - \vec{v_\infty} )
$$
{%endraw%}

In order to set $$\nabla \mathscr{L} = 0$$, the gradient of the Lagrangian has the components:

{%raw%}
$$
\begin{align}
\frac{\partial \mathscr{L}}{\partial \vec{\lambda_{v_\infty}}} & = \vec{v_\infty^*} - \vec{v_\infty} \\
\frac{\partial \mathscr{L}}{\partial t_1} & = \vec{\lambda_{v_\infty}} \cdot \frac{\partial \vec{v_\infty}}{\partial t_1} \\
\frac{\partial \mathscr{L}}{\partial \Delta\vec{v_1}} & = \frac{\Delta\vec{v_1}}{\left\| \Delta\vec{v_1} \right\|} + \vec{\lambda_{v_\infty}} \cdot \frac{\partial \vec{v_\infty}}{\partial \Delta\vec{v_1}}
\end{align}
$$
{%endraw%}

Where:

{%raw%}
$$
\begin{align}
\frac{\partial \vec{v_\infty}}{\partial t_1} = \frac{\partial \vec{v_\infty}}{\partial \vec{r_1}} \frac{\partial \vec{r_1}}{\partial t_1} + \frac{\partial \vec{v_\infty}}{\partial \vec{v_1^+}} \frac{\partial \vec{v_1^+}}{\partial t_1}
\end{align}
$$
{%endraw%}

{%raw%}
$$
\begin{align}
\frac{\partial \vec{v_\infty}}{\partial \Delta\vec{v_1}} & = \frac{\partial \vec{v_\infty}}{\partial \vec{r_1}} \frac{\partial \vec{r_1}}{\partial \Delta\vec{v_1}} + \frac{\partial \vec{v_\infty}}{\partial \vec{v_1^+}} \frac{\partial \vec{v_1^+}}{\partial \Delta\vec{v_1}} \\
 & = \frac{\partial \vec{v_\infty}}{\partial \vec{v_1^+}}
\end{align}
$$
{%endraw%}

The partials with respect to $$\vec{v_\infty}$$ can be found by using automatic differentiation with the formulas:

{%raw%}
$$
\begin{align}
v_\infy = \sqrt{ - \mu / a }
a = 1 / ( 2 / r - v^2 / \mu )
\vec{v_\infty} = 
\end{align}
$$
{%endraw%}
